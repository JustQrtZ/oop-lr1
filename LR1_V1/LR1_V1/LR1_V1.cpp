﻿#include <iostream>
#include <conio.h>
#include <string.h>
#include <typeinfo>
using namespace std;


template<typename T>
class myClass
{
private:
	int size;
	T* arr;
public:
	friend ostream& operator << <T> (ostream& str, const myClass<T>& ov);
	myClass()
	{
		size = 0;
		arr = new T[size];
		get();
	}
	myClass(int size, bool rnd)
	{
		this->size = size;
		arr = new T[size];

		(rnd) ? random(arr) : get();
	}
	void random(T* arr)
	{
		for (int i = 0; i <= size; i++)
		{
			arr[i] = rand() % 10;
		}
	}
	myClass(const myClass& obj)
	{
		size = obj.size;
		for (int i = 0; i < size; ++i)
			arr[i] = obj.arr[i];
	}
	void setsize()
	{
		std::cout << "Vvedite razmernost " << std::endl;
		std::cin >> size;
		arr = new T[size];
	}
	void get()
	{
		if (!size)
		{
			setsize();
		}
		else
		{
			for (int i = 0; i < size; i++)
			{
				std::cout << "Vvedite element [" << i << "] ";
				std::cin >> arr[i];
			}
		}
	}
	void addelements(std::initializer_list<int> data)
	{
		T* tmp = new T[size + data.size()];
		this->size += data.size();
		for (int i = 0; i < size - data.size(); i++)
		{
			tmp[i] = arr[i];
		}
		delete[] arr;
		arr = tmp;
		int i = size - data.size();
		for (auto j : data)
		{
			arr[i] = j;
			i++;
		}
		cout << size;
	}
	void deleteElementByIndex(int index)
	{
		size--;
		T* tmp = new T[size];

		for (int i = 0, j = 0; i < size + 1; i++)
		{
			if (i != index)
			{
				tmp[j] = arr[i];
				j++;
			}
		}
		delete[] arr;
		arr = tmp;
	}
	void findByvalue(int value)
	{
		cout << "Find elements with value " << value << endl;
		for (int i = 0; i < size; i++)
		{
			if (arr[i] == value)
			{
				cout << "Element Value " << arr[i] << " Index " << i << endl;
			}
		}
	}
	void shellSort() {
		int gap, j, k;
		for (gap = size / 2; gap > 0; gap = gap / 2) {        //initially gap = n/2,
			for (j = gap; j < size; j++) {
				for (k = j - gap; k >= 0; k -= gap) {
					if (arr[k + gap] >= arr[k])
						break;
					else
						swapping(arr[k + gap], arr[k]);
				}
			}
		}
	}
	void swapping(int& a, int& b) {        //swap the content of a and b
		int temp;
		temp = a;
		a = b;
		b = temp;
	}
};

template<typename T>
ostream& operator << (ostream& str, const myClass<T>& ov)
{
	for (int i = 0; i < ov.size; i++)
	{
		str << "Element [" << i << "] = " << ov.arr[i] << endl;
	}

	return str;
}

int main()
{
	myClass<int> d(10, true);
	myClass<int> test(10, false);
	cout << test;
	cout << d;
	test.addelements({ 1 , 2, 3, 4 });
	cout << test;
	test.deleteElementByIndex(0);
	cout << test;
	test.findByvalue(1);
	test.shellSort();
	cout << test;
}

